# Augmented-Medical-Information

AR mobile App to visualise the data flow of medical information systems in the Living Lab at the Bern University of Applied Sciences in Biel, Switzerland.

Beta-Release: 27.1.2020

Demo-Video on [YouTube](https://www.youtube.com/watch?v=Uz7N27ZwNHk&ab_channel=JoshuaDrewlow)

Developers:

- Joshua Drewlow
- Guillaume Fricker
- Daniel Reichenpfader

Tech stack:

- Unity
- Google ARCore

Documentation: https://wiki.ti.bfh.ch/display/Medizininformatik/AMI+-+Augmented+Medical+Information
